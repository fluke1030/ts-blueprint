import './adapters/amqp';
import { AmqpHealthCheck } from './adapters/amqp/amqp.health';
import { config } from './bootstrapConfig';
import { defaultLogger } from './bootstrapLogger';
import { traceUtil } from './bootstrapTracer';
import { createAmqpConnection, IAmqpConnectionOptions } from './libraries/amqp';
import { Healthchecker } from './libraries/health/healthchecker';
import { gracefulShutdown } from './libraries/shutdown/gracefulShutdown';
import { ensureConfigKeys } from './utils/configUtil';
import { getDependency } from './utils/diUtil';

async function bootstrapAmqp() {
  try {
    ensureConfigKeys(config.amqp, 'AMQP_URI')

    const options: IAmqpConnectionOptions = {
      connectionUrl: config.amqp.AMQP_URI!,
      username: config.amqp.AMQP_USERNAME,
      password: config.amqp.AMQP_PASSWORD,
      prefix: config.amqp.AMQP_PREFIX,
      defaultReplyTimeout: config.amqp.AMQP_DEFAULT_REPLY_TIMEOUT,
    }

    const connection = await createAmqpConnection(options, defaultLogger, traceUtil);

    defaultLogger.info({ event: 'amqp_connected' }, `Successfully connect to amqp server`);

    const healthcheck = getDependency(Healthchecker);
    healthcheck.addChecker(new AmqpHealthCheck(connection));

    gracefulShutdown(defaultLogger, 'amqp', ['http'], async () => {
      await connection.close()
    });

  } catch (error) {
    defaultLogger.error(error, { event: 'bootstrap_amqp' })

    process.exit(-1)
  }
}

bootstrapAmqp();