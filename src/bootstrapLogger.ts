import Container from 'typedi';
import { config } from './bootstrapConfig';
import { JsonLogger } from "./libraries/logger/json.logger";

export const defaultLogger = new JsonLogger(config.APP_NAME, {
  level: config.LOG_LEVEL,
  appVersion: config.APP_VERSION
})

// register jsonLogger to di to support @Inject('logger') private logger: ILogger
Container.set([{ id: "logger", value: defaultLogger }])