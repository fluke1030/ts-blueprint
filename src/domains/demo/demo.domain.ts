import { Validator } from 'class-validator';
import { Inject, Service } from 'typedi';
import { RequestError } from '../../errors/errors';
import { ILogger } from '../../libraries/logger/logger.interface';
import { consoleLogger } from '../../logger';
import { IDemoRepository } from '../../repositories/demo/demo.repository';
import { PastDateError } from './errorTypes';
import { IDemo, IDemoRequest } from './interface';

@Service()
export class DemoDomain {
  constructor(
    @Inject('IDemoRepository') private repo: IDemoRepository,
    @Inject('logger') private logger: ILogger = consoleLogger
  ) { }

  private validateDemoRequest(request: IDemoRequest) {
    const validator = new Validator()

    const now = new Date()

    if (!validator.isEmail(request.authorEmail)) {
      throw new RequestError(`${request.authorEmail} is not an email`)
    }

    if (!validator.minDate(request.presentDate, now)) {
      throw new PastDateError('presentDate', request.presentDate)
    }
  }

  async createDemo(request: IDemoRequest): Promise<IDemo> {
    this.logger.debug({ event: 'create_demo', request_name: request.name }, 'Start creating demo')

    this.validateDemoRequest(request)

    const demo = await this.repo.create(request)

    this.logger.debug({ event: 'create_demo', id: demo._id }, 'End creating demo')

    return demo
  }

  async findAll(): Promise<IDemo[]> {
    return this.repo.findAll()
  }

}

