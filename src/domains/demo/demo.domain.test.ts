import { mock, mockReset } from 'jest-mock-extended';
import { RequestError } from '../../errors/errors';
import { IDemoRepository } from '../../repositories/demo/demo.repository';
import { DemoDomain } from './demo.domain';
import { PastDateError } from './errorTypes';

describe('demo domain', () => {
  const mockRepo = mock<IDemoRepository>()

  beforeEach(() => {
    mockReset(mockRepo)
  })

  const demoDomain = new DemoDomain(mockRepo)

  test('should create demo successfully', async () => {
    const demo = {
      name: 'test',
      authorEmail: 'test@email.com',
      presentDate: new Date(Date.now() + 1000)
    }

    mockRepo.create.mockResolvedValue({
      _id: '1',
      ...demo
    })

    const createdDemo = await demoDomain.createDemo(demo)

    expect(createdDemo).toBeTruthy()
    expect(createdDemo._id).toBe('1')
  })

  test('should error when presentDate is in the past', async () => {
    async function testFn() {
      return demoDomain.createDemo({
        name: 'test',
        authorEmail: 'test@email.com',
        presentDate: new Date(Date.now() - 1000)
      })
    }

    expect(testFn()).rejects.toBeInstanceOf(PastDateError)
  })

  test('should error when authorEmail is not and email', async () => {
    async function testFn() {
      return demoDomain.createDemo({
        name: 'test',
        authorEmail: 'testemail.com',
        presentDate: new Date(Date.now() + 1000)
      })
    }

    expect(testFn()).rejects.toBeInstanceOf(RequestError)
  })

})