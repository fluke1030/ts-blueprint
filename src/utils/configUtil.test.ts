import { ensureConfigKey, ensureConfigKeys } from "./configUtil";

class MockConfig {
  key1: string = '';
  key2: number = 0;
}

describe('configUtil.test', () => {

  describe('Single key', () => {

    it('should not throw error when key exists', () => {
      const config = new MockConfig();
      config.key1 = 'test';

      const check = () => {
        ensureConfigKey(config, 'key1')
      }

      expect(check).not.toThrow();
    })

    it('should throw error when key does not exists', () => {
      const config = new MockConfig();

      const check = () => {
        ensureConfigKey(config, 'key1')
      }

      expect(check).toThrow();
    })


  })

  describe('Multiple keys', () => {

    it('should not throw error when keys exists', () => {
      const config = new MockConfig();
      config.key1 = 'test';
      config.key2 = 1;

      const check = () => {
        ensureConfigKeys(config, 'key1', 'key2');
      }

      expect(check).not.toThrow();
    })

    it('should throw error when keys do not exists', () => {
      const config = new MockConfig();
      config.key1 = 'test';

      const check = () => {
        ensureConfigKeys(config, 'key1', 'key2');
      }

      expect(check).toThrow();
    })


  })

})