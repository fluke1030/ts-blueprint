import { defaultLogger } from "./bootstrapLogger";
import { CacheManager } from './libraries/cache/cacheManager';
import { InmemoryCacheProvider } from './libraries/cache/providers/inmemoryCacheProvider';

async function bootstrapCache() {
  const logger = defaultLogger;

  try {
    // setup cache provider
    const inmemoryProvider = new InmemoryCacheProvider(logger, { max: 500, ttl: 30 }); // 30 seconds

    // ensureConfigKey(config, 'REDIS_URL');
    // const redisProvider = new RedisCacheProvider(logger, { ttl: 360, redisUrl: config.REDIS_URL }); // 1 hour
    // await redisProvider.connect();
    // const healthcheck = getDependency(Healthchecker);
    // healthcheck.addChecker(new RedisHealthCheck(redisProvider.getRedisClient()));

    // const multiTierCache = new MultiTiersCacheProvider(logger, inmemoryProvider.cache, redisProvider.cache);

    CacheManager.NEW_INSTANCE(logger, inmemoryProvider);

    logger.info({ event: 'bootstrap_cache' }, 'Successfully bootstrap');
  } catch (err) {
    logger.error(err, { event: 'bootstrap_cache' }, 'Error booting cached');
    process.exit(-1);
  }

}

bootstrapCache();