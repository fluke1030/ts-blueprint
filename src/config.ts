import { Env, Load } from '@tel/env-decorator';
import * as dotenv from 'dotenv';
import { ILogLevel } from './libraries/logger/json.logger';

dotenv.config();

class MongoConfig {
  @Env(['MONGO_URI'])
  MONGO_URI: string | undefined;

  @Env(['MONGO_USERNAME'])
  MONGO_USERNAME: string | undefined;

  @Env(['MONGO_PASSWORD'])
  MONGO_PASSWORD: string | undefined;

  @Env(['MONGO_DATABASE_NAME'])
  MONGO_DATABASE_NAME: string | undefined;
}

class AmqpConfig {
  @Env(['AMQP_URI'])
  AMQP_URI: string | undefined;

  @Env(['AMQP_USERNAME'])
  AMQP_USERNAME: string | undefined;

  @Env(['AMQP_PASSWORD'])
  AMQP_PASSWORD: string | undefined;

  @Env(['AMQP_DEFAULT_REPLY_TIMEOUT'])
  AMQP_DEFAULT_REPLY_TIMEOUT: number = 2500;

  @Env(['NODE_ENV'])
  AMQP_PREFIX: string | undefined;
}

class KafkaConfig {
  @Env(['KAFKA_BROKERS'])
  KAFKA_BROKERS: string | undefined;

  @Env(['KAFKA_TOPIC_PREFIX'])
  KAFKA_TOPIC_PREFIX: string | undefined;

  @Env(['KAFKA_USERNAME'])
  KAFKA_USERNAME: string | undefined;

  @Env(['KAFKA_PASSWORD'])
  KAFKA_PASSWORD: string | undefined;

  @Env(['KAFKA_MECHANISM'])
  KAFKA_MECHANISM: string | undefined


  @Env(['KAFKA_SASL'])
  KAFKA_SASL: boolean | undefined = false

  @Env(['KAFKA_SSL'])
  KAFKA_SSL: boolean | undefined = false
}

export class AppConfig {
  @Env(['APP_NAME', 'npm_package_name'], { required: true })
  APP_NAME!: string;

  @Env(['npm_package_description'])
  APP_DESCRIPTION: string = '';

  @Env(['npm_package_version'])
  APP_VERSION: string = '';

  @Env(['LOG_LEVEL'])
  LOG_LEVEL: ILogLevel | undefined;

  @Env()
  SERVER_ENABLED: boolean = true;

  @Env()
  SERVER_PORT: number = 8080;

  @Env()
  SERVER_HOST: string = '0.0.0.0';

  @Load()
  database!: MongoConfig

  @Load()
  amqp!: AmqpConfig

  @Load()
  kafka!: KafkaConfig

  @Env(['ECHO_URL'])
  ECHO_URL: string | undefined;

  @Env(['JWT_SECRET'])
  JWT_SECRET: string | undefined;

  @Env(['REDIS_URL'])
  REDIS_URL: string | undefined;
}