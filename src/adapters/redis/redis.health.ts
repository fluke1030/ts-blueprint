import { Redis } from 'ioredis';
import { IHealthCheck } from '../../libraries/health/interface';

export class RedisHealthCheck implements IHealthCheck {
  constructor(private redisClient: Redis) {};

  getName() {
    return 'redis';
  }

  async getHealth() {
    const status = this.redisClient.status;
    const ok = status === 'ready';

    return {
      ok,
      status
    }
  }
}