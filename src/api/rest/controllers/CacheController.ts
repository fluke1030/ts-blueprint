import { Controller, Get, Post, QueryParam } from 'routing-controllers';
import { CacheSampleRepo } from '../../../libraries/cache/example/cachesample.repo';

@Controller('/cache')
export class CacheController {
  constructor(private sampleRepo: CacheSampleRepo) { }

  @Get('/sample1')
  getSample(
    @QueryParam('id', { required: true }) id: string,
    @QueryParam('text', { required: true }) text: string) {
    return this.sampleRepo.getSampleById1(id, text);
  }

  @Post('/sample1')
  updateSample1(
    @QueryParam('id', { required: true }) id: string,
    @QueryParam('text', { required: true }) text: string) {
    return this.sampleRepo.updateSample1({ id, text });
  }

  @Get('/sample2')
  getSample2(
    @QueryParam('id', { required: true }) id: string,
    @QueryParam('text', { required: true }) text: string) {
    return this.sampleRepo.getSampleById2(id, text);
  }

  @Post('/sample2')
  updateSample2(
    @QueryParam('id', { required: true }) id: string,
    @QueryParam('text', { required: true }) text: string) {
    return this.sampleRepo.updateSample2(id, text);
  }
}
