import { IRouterContext } from 'koa-router';
import { Controller, Ctx, Get, QueryParam, UseBefore } from 'routing-controllers';
import { DemoMongoRepository } from '../../../adapters/mongo/repositories/demo.mongo.repository';
import { enableTracing } from '../../../bootstrapRestApi';

@Controller('/echo')
export class EchoController {
  constructor(private demoRepo: DemoMongoRepository) { }

  @Get()
  @UseBefore(enableTracing)
  async echo(@Ctx() ctx: IRouterContext, @QueryParam('error') error: string) {
    const { headers, path, query } = ctx.request

    if (error) {
      throw new Error('Forced error')
    }

    const results = await this.demoRepo.findAll();
    console.log('results: ', results.length)

    const result = await this.demoRepo.findByName('test');
    console.log('result: ', result)

    return { headers, path, query, results }
  }

}
