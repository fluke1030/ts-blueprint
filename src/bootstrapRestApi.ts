import { RestApiServer } from './api/rest';
import { globalTracingMiddleware } from './api/rest/middlewares/tracing';
import { config } from './bootstrapConfig';
import { defaultLogger } from './bootstrapLogger';
import { traceUtil } from './bootstrapTracer';
import { AuthenticationProvider } from './libraries/authentication/authenticationProvider';
import { IAuthorizationProvider } from './libraries/authorization/type';
import { IUser } from './repositories/user/user.repository';
import { ensureConfigKeys } from './utils/configUtil';
import { getOptionalDependency } from './utils/diUtil';

export const enableTracing = globalTracingMiddleware(traceUtil)

try {
  if (config.SERVER_ENABLED) {
    ensureConfigKeys(config, 'SERVER_HOST', 'SERVER_PORT')

    const authenticationProvider = getOptionalDependency<AuthenticationProvider<IUser>>(AuthenticationProvider);
    const authorizationProvider = getOptionalDependency<IAuthorizationProvider<IUser>>('AuthorizationProvider');

    const restApiServer = new RestApiServer({
      port: config.SERVER_PORT,
      hostname: config.SERVER_HOST,
      logger: defaultLogger,
      appName: config.APP_NAME,
      appDescription: config.APP_DESCRIPTION,
      appVersion: config.APP_VERSION,
      authenticationProvider,
      authorizationProvider
    });

    restApiServer.start();
  }
} catch (error) {
  defaultLogger.error(error, { event: 'bootstrap_restapi' })

  process.exit(-1)
}