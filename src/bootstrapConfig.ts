import { loadConfig } from '@tel/env-decorator';
import Container from 'typedi';
import { AppConfig } from './config';
import { objectToDiValues } from './utils/diUtil';

export const config = loadConfig(AppConfig)

// register values to support @Inject('config.CONFIG_KEY') private key: any
Container.set(objectToDiValues(config, 'config.'))