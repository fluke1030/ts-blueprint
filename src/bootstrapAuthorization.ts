import Container from 'typedi';
import { AggregateAuthorizationProvider } from './libraries/authorization/aggregateProvider';
import { AuthenticatedAuthorizationProvider } from './libraries/authorization/authenticatedProvider';
import { PermissionAuthorizationProvider } from './libraries/authorization/permissionProvider';
import { RoleAuthorizationProvider } from './libraries/authorization/roleProvider';
import { IPermissionRepository } from './libraries/authorization/type';
import { IUser } from './repositories/user/user.repository';

const roleProvider = new RoleAuthorizationProvider()
const authenticatedProvider = new AuthenticatedAuthorizationProvider()

const permissionRepo = Container.get<IPermissionRepository<IUser>>('IPermissionRepository')
const permissionProvider = new PermissionAuthorizationProvider(permissionRepo)

export const authorizationProvider = new AggregateAuthorizationProvider(
  [roleProvider, authenticatedProvider, permissionProvider]
);

Container.set({
  id: 'AuthorizationProvider',
  value: authorizationProvider
})