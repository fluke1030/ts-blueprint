import { CacheManager } from '../cacheManager';
import { cache } from './cache';
import { cacheKey } from './cacheKey';
import { CacheMeta, ICacheMeta } from './meta';


jest.mock('../cacheManager');

describe('@cache decorator', () => {
  const mockWrapCache = CacheManager.WRAP_CACHE as jest.Mock;

  beforeEach(() => {
    mockWrapCache.mockReset();
    CacheMeta.cacheKeyMetaStorage = {};
  })

  it('should wrap cache with method name as key', () => {
    // @ts-ignore
    class TestClass {
      @cache()
      async test1(_id: string) { }
    }

    assertWrapCacheCall('test1', {
      cacheOption: { key: 'test1' },
      cacheKeyMetas: []
    })
  })

  it('should wrap cache using options', () => {
    // @ts-ignore
    class TestClass {
      @cache({ key: 'key1', ttl: 5, max: 50 })
      async test1(_id: string) { }
    }

    assertWrapCacheCall('test1', {
      cacheOption: { key: 'key1', ttl: 5, max: 50 },
      cacheKeyMetas: []
    })
  })

  it('should wrap cache with empty @cacheKey', () => {
    // @ts-ignore
    class TestClass {
      @cache()
      async test1(@cacheKey() _id: string) { }
    }

    assertWrapCacheCall('test1', {
      cacheOption: { key: 'test1' },
      cacheKeyMetas: [{ parameterIndex: 0 }]
    })
  })

  it('should wrap cache with @cacheKey with property key', () => {
    // @ts-ignore
    class TestClass {
      @cache()
      async test1(@cacheKey('id') _obj: { id: string }) { }
    }

    assertWrapCacheCall('test1', {
      cacheOption: { key: 'test1' },
      cacheKeyMetas: [{ parameterIndex: 0, propertyKey: 'id' }]
    })
  })

  function assertWrapCacheCall(expectedFunctionName: string, expectMeta: ICacheMeta) {
    expect(mockWrapCache).toHaveBeenCalledTimes(1);

    const call1 = mockWrapCache.mock.calls[0];
    expect(call1).toHaveLength(2);

    const arg1 = call1[0];
    const arg2 = call1[1];

    expect(arg1).toBeDefined();
    expect(typeof arg1).toBe('function');
    expect(arg1.name).toBe(expectedFunctionName);
    expect(arg2).toBeDefined();
    expect(arg2).toEqual(expectMeta);
  }

})
