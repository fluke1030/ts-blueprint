import { ICacheEvictOptions, ICacheOptions } from '../interface';

export interface ICacheKeyMeta {
  parameterIndex: number
  propertyKey?: string
}

export interface ICacheMeta {
  cacheOption: ICacheOptions<any>
  cacheKeyMetas: ICacheKeyMeta[]
}

export interface ICacheEvictMeta {
  cacheEvictOption: ICacheEvictOptions<any>
  cacheKeyMetas: ICacheKeyMeta[]
}

export class CacheMeta {
  static cacheKeyMetaStorage: { [key: string]: ICacheKeyMeta[] } = {};

  private static getKey(target: Object, propertyKey: string) {
    return `${target.constructor.name}-${propertyKey}`;
  }

  static GET_CACHE_META(target: Object, propertyKey: string, cacheOption: ICacheOptions<any>): ICacheMeta {
    const key = this.getKey(target, propertyKey);

    const cacheKeyMetas = this.cacheKeyMetaStorage[key] || [];

    return { cacheOption, cacheKeyMetas };
  }

  static GET_CACHE_EVICT_META(target: Object, propertyKey: string, cacheEvictOption: ICacheEvictOptions<any>): ICacheEvictMeta {
    const key = this.getKey(target, propertyKey);

    const cacheKeyMetas = this.cacheKeyMetaStorage[key] || [];

    return { cacheEvictOption, cacheKeyMetas };
  }

  static ADD_CACHEKEY_META(
    target: Object,
    propertyKey: string,
    parameterIndex: number,
    cachePropKey?: string
  ) {
    const key = this.getKey(target, propertyKey);

    const meta: ICacheKeyMeta = { parameterIndex, propertyKey: cachePropKey };

    const existing = this.cacheKeyMetaStorage[key];

    if (!existing) {
      this.cacheKeyMetaStorage[key] = [meta];
    } else {
      existing.push(meta);
    }

  }

}