import * as cacheManager from 'cache-manager';
import * as redisStore from 'cache-manager-ioredis';
import { ClusterNode, ClusterOptions, RedisOptions } from 'ioredis';
import { parseURL } from 'ioredis/built/utils';
import { ILogger } from '../../logger/logger.interface';
import { ICacheConfig, ICacheProvider } from '../interface';

export interface IRedisCacheOptions {
  ttl: number
  redisUrl?: string
  clusterConfig?: {
    nodes: ClusterNode[],
    options?: ClusterOptions
  }
}

export class RedisCacheProvider implements ICacheProvider {
  cache!: cacheManager.Cache;
  private config: cacheManager.StoreConfig & cacheManager.CacheOptions & RedisOptions;

  constructor(private logger: ILogger, options: IRedisCacheOptions) {
    const config = {
      ttl: options.ttl,
      clusterConfig: options.clusterConfig
    };

    if (options.redisUrl) {
      Object.assign(config, parseURL(options.redisUrl));
    }

    logger.debug(config, 'RedisCacheProvider options');

    this.config = { store: redisStore, ...config };
  }

  getRedisClient() {
    const store = this.cache?.store as any;

    return store?.getClient()
  }

  async connect() {
    const redisCache = cacheManager.caching(this.config);
    this.cache = redisCache;

    const redisClient = this.getRedisClient()

    return new Promise((resolve, reject) => {
      redisClient.on('connect', () => {
        this.logger.debug('RedisCacheProvider connected');
        resolve();
      });

      redisClient.on('error', (err: any) => {
        this.logger.error(err, 'RedisCacheProvider error');
        reject(new Error(err));
      });
    });
  }

  wrap(key: string, cb: () => any, option?: ICacheConfig): Promise<any> {
    return this.cache.wrap(key, cb, option as any);
  }

  del(key: string, cb: (err: any) => void) {
    return this.cache.del(key, cb);
  }

}