import * as cacheManager from 'cache-manager';
import { ILogger } from '../../logger/logger.interface';
import { ICacheConfig, ICacheProvider } from '../interface';

export class MultiTiersCacheProvider implements ICacheProvider {
  private cache!: cacheManager.MultiCache;

  constructor(logger: ILogger, ...caches: cacheManager.Cache[]) {
    logger.debug('MultiTiersCacheProvider with tiers: ', caches.length);

    this.cache = cacheManager.multiCaching(caches);
  }

  wrap(key: string, cb: () => any, option?: ICacheConfig): Promise<any> {
    return this.cache.wrap(key, cb, option as any);
  }

  del(key: string, cb: (err: any) => void) {
    return this.cache.del(key, cb);
  }

}