import * as bunyan from 'bunyan'
import bunyanFormat from 'bunyan-format'
import { ILogger, LogType } from './logger.interface'

export type ILogLevel = 'trace' | 'debug' | 'info' | 'warn' | 'error' | 'fatal'

export interface ITracer {
    getLocalTracingKey: () => string
    getLocalTracingId: () => string | undefined
}

export interface ILoggerOption {
    formatOutput?: boolean
    lastRawStream?: NodeJS.WritableStream
    level?: ILogLevel
    appVersion?: string
}

function addData(obj: any, params: any[] = [], moreObj: any) {
    if (!moreObj) {
        return [obj, params]
    }

    if (obj instanceof Error) {
        const newObj = Object.assign({ err: obj }, moreObj)
        return [newObj, params]
    }

    if (typeof obj === 'object') {
        const newObj = Object.assign({}, obj, moreObj)
        return [newObj, params]
    }


    return [moreObj, [obj, ...params]]
}

export class JsonLogger implements ILogger {
    private logger: bunyan
    tracer?: ITracer

    constructor(name: string, options: ILoggerOption = {}) {
        const { level, appVersion } = options

        const logOptions: bunyan.LoggerOptions = {
            name,
            level,
            serializers: { err: bunyan.stdSerializers.err }
        }

        if (appVersion) {
            logOptions.appVersion = appVersion
        }

        // replace default stream to output formatting stream
        if (options.formatOutput) {
            logOptions.stream = bunyanFormat({ outputMode: 'short' })
        }

        this.logger = bunyan.createLogger(logOptions)

        // add last stream to stream list
        if (options.lastRawStream) {
            this.logger.addStream({
                level: level,
                stream: options.lastRawStream,
                type: 'raw'
            })
        }
    }

    private getTraceData() {
        if (this.tracer) {
            return { [this.tracer.getLocalTracingKey()]: this.tracer.getLocalTracingId() }
        }

        return undefined
    }

    private log(type: LogType, obj: any, params: any[] = []) {
        const traceData = this.getTraceData()

        if (obj instanceof Error && params.length > 0 && typeof params[0] === 'object') {
            obj = Object.assign({ err: obj }, params[0])
            params = params.slice(1)
        }

        const [newObj, newParams] = addData(obj, params, traceData)

        this.logger[type](newObj, ...newParams)
    }

    info(error: Error, ...params: any[]): void
    info(obj: Object, ...params: any[]): void

    info(obj: any, ...params: any[]) {
        this.log('info', obj, params)
    }

    debug(error: Error, ...params: any[]): void
    debug(obj: Object, ...params: any[]): void

    debug(obj: any, ...params: any[]) {
        this.log('debug', obj, params)
    }

    warn(error: Error, ...params: any[]): void
    warn(obj: Object, ...params: any[]): void

    warn(obj: any, ...params: any[]) {
        this.log('warn', obj, params)
    }

    error(error: Error, ...params: any[]): void
    error(obj: Object, ...params: any[]): void

    error(obj: any, ...params: any[]) {
        this.log('error', obj, params)
    }

    trace(error: Error, ...params: any[]): void
    trace(obj: Object, ...params: any[]): void

    trace(obj: any, ...params: any[]) {
        this.log('trace', obj, params)
    }
}