
import { EventEmitter } from 'events';
import { mock, mockClear } from 'jest-mock-extended';
import { BrokerAsPromised as Broker, BrokerConfig } from 'rascal';
import { TraceUtil } from "../../libraries/tracing/traceUtil";
import { ILogger } from '../logger/logger.interface';
import * as errorHandler from "./errorHandler";
import { IPublishEventemitter, IRascalBroker, ISubscriptionSession, RascalBroker } from './rascalBroker';
import { IPublicationConfig, IQueueConfig, ISubscriptionConfig } from './rascalConfig';
import { PublicationMessage } from './type';


jest.mock("rascal")
jest.mock('./errorHandler.ts')


const errorStrategyMock: ISubscriptionConfig = { errorStrategy: { type: 'REJECT' } }

const subscriptionConfigMock: IQueueConfig = {
  queueName: 'test-queue',
  exchange: 'test-exchange',
  routingKey: "routing.*",
  exchangeConfig: { type: "topic" }
}

const messageSubscribeMock = {
  properties: {
    message_id: "test-message-id",
    headers: {
      "uber-trace-id": "7572096c578382ee:7572096c578382ee:0:1",
      "x-correlation-id": "c0bd7caa-6b43-41c6-92a0-c4951ddcf0d0"
    }
  }
}

const contentMock = 'test-content'



describe("RascalBroker", () => {


  const ackOrNackMock = jest.fn()
  let publicationEvent: any
  let subscriptionEvent: any
  const handleOnMessageErrorMock = errorHandler.handleOnMessageError as jest.Mock
  const brokerCreateMock = Broker.create as jest.Mock
  const brokerConfig = mock<BrokerConfig>()
  const logger = mock<ILogger>()
  const rascalBrokerMock = mock<IRascalBroker>()
  const traceUtil = mock<TraceUtil>()

  function buildPublication (): IPublishEventemitter {
    const publicationEvent = new EventEmitter() as IPublishEventemitter
    // if this publish function has been called will return eventEmitter
    rascalBrokerMock.publish.mockResolvedValue(publicationEvent)

    return publicationEvent
  }

  function buildSubscription (): ISubscriptionSession {
    const subscriptionEvent = new EventEmitter() as  ISubscriptionSession
    // if this subscribe function has been called will return eventEmitter
    rascalBrokerMock.subscribe.mockResolvedValue(subscriptionEvent)

    return subscriptionEvent
  }

  beforeEach(() => {
    publicationEvent = new EventEmitter() as IPublishEventemitter
    subscriptionEvent = new EventEmitter() as ISubscriptionSession
    brokerCreateMock.mockResolvedValue(rascalBrokerMock)
  })

  afterEach(() => {
    mockClear(brokerConfig)
    mockClear(logger)
    mockClear(rascalBrokerMock)
    mockClear(traceUtil)
    brokerCreateMock.mockClear()
    ackOrNackMock.mockClear()
    handleOnMessageErrorMock.mockClear()
    publicationEvent.removeAllListeners()
    subscriptionEvent.removeAllListeners()
  })



  it(`should publish message successfully`, async () => {
    const rascalBroker = new RascalBroker(brokerConfig, logger, traceUtil);
    await rascalBroker.connect()

    rascalBrokerMock.publish.mockResolvedValue(subscriptionEvent)

    const messageMock: PublicationMessage = { "msg": "hello-message" }
    const publicationConfigMock = <IPublicationConfig>{
      queue: "test-queue",
      exchange: "test-exchange",
      routingKey: "routing-test",
      exchangeConfig: {
        type: "topic",
      },
    }

    const pub = rascalBroker.publish(publicationConfigMock, messageMock)

    process.nextTick(() => {
      subscriptionEvent.emit('success', "message-1")
    })

    expect(rascalBroker).toBeDefined()
    expect(brokerCreateMock).toHaveBeenCalled()
    expect(rascalBrokerMock.publish).toHaveBeenCalledWith('test-exchange', { "msg": "hello-message" }, {
      queue: "test-queue", exchange: "test-exchange", routingKey: "routing-test", options: { headers: {} }, exchangeConfig: { type: "topic" },
    })
    expect(traceUtil.injectTracingContext).toHaveBeenCalled()

    await expect(pub).resolves.toBe('message-1')
  })


  it(`should publish message and reply successfully`, async () => {
    const rascalBroker = new RascalBroker(brokerConfig, logger, traceUtil);
    await rascalBroker.connect()

    // emit the 'message' event on subscription function
    const subscriptionEvent = buildSubscription()
    subscriptionEvent.emit('message', { properties: { messageId: "message-1" } }, contentMock, ackOrNackMock)

    const publicationEvent = buildPublication()
    // publish message to a queue and wait to reply
    const reply = rascalBroker.publish(
      { queue: "test-queue", options: { replyTo: "reply-queue", correlationId: "correlation-1" }, replyTimeout: 100 },
      { "msg": "test-content" },
    )

    process.nextTick(() => {
      // emit the 'success' event on publish function
      publicationEvent.emit('success', "message-1")

      // emit the 'message' event on subscripe function
      subscriptionEvent.emit('message', { properties: { messageId: "message-1", correlationId: "correlation-1", replyTo: "reply-queue" } }, 'test-content', ackOrNackMock)
    })

    await expect(reply).resolves.toBe('test-content')
  })

  it(`should publish message and reply fail when correlationId does not match`, async () => {
    const rascalBroker = new RascalBroker(brokerConfig, logger, traceUtil);
    await rascalBroker.connect()

    const subscriptionEvent = buildSubscription()
    subscriptionEvent.emit('message', { properties: { messageId: "message-1" } }, contentMock, ackOrNackMock)

    const publicationEvent = buildPublication()
    // publish message to a queue and wait to reply
    const reply = rascalBroker.publish(
      { queue: "test-queue", options: { replyTo: "reply-queue", correlationId: "correlation-1" }, replyTimeout: 100 },
      { "msg": "test-content" },
    )

    process.nextTick(() => {
      // emit the 'success' event on publish function
      publicationEvent.emit('success', "message-1")

      // emit the 'message' event on subscribe function
      subscriptionEvent.emit('message', { properties: { messageId: "message-1", correlationId: "correlation-2xy", replyTo: "reply-queue" } }, 'test-content', ackOrNackMock)
    })

    await expect(reply).rejects.toBe('reply queue receive timeout on reply-queue-correlation-1')

    expect(logger.debug).toHaveBeenCalled()
    expect(logger.debug.mock.calls[0][0]).toBe("message key reply-queue-correlation-2xy does not match on pending replies")
  })

  it(`should publish message and reply fail when replying timeout`, async () => {
    const rascalBroker = new RascalBroker(brokerConfig, logger, traceUtil);
    await rascalBroker.connect()

    const publicationEvent = buildPublication()
    // publish message to a queue and wait to reply
    const reply = rascalBroker.publish(
      { queue: "test-queue", options: { replyTo: "reply-queue", correlationId: "correlation-1" }, replyTimeout: 100 },
      { "msg": "hello" },
    )

    process.nextTick(() => {
      // emit the 'success' event on publish function
      publicationEvent.emit('success', "message-1")
    })

    await expect(reply).rejects.toBe('reply queue receive timeout on reply-queue-correlation-1')
  })

  it(`should publish message successfully when not set tracer`, async () => {
    const rascalBroker = new RascalBroker(brokerConfig, logger);
    await rascalBroker.connect()

    rascalBrokerMock.publish.mockResolvedValue(subscriptionEvent)

    const messageMock: PublicationMessage = { "msg": "hello-message" }
    const publicationConfigMock = <IPublicationConfig>{
      queue: "test-queue",
      exchange: "test-exchange",
      routingKey: "routing-test",
      exchangeConfig: {
        type: "topic",
      },
    }

    const pub = rascalBroker.publish(publicationConfigMock, messageMock)

    process.nextTick(() => {
      subscriptionEvent.emit('success', "message-1")
    })


    expect(rascalBroker).toBeDefined()
    expect(brokerCreateMock).toHaveBeenCalled()
    expect(rascalBrokerMock.publish).toHaveBeenCalledWith('test-exchange', { "msg": "hello-message" }, {
      queue: "test-queue", exchange: "test-exchange", routingKey: "routing-test", exchangeConfig: { type: "topic" },
    })
    expect(traceUtil.injectTracingContext).not.toHaveBeenCalled()

    await expect(pub).resolves.toBe('message-1')
  })

  it(`should publish message with overridden configuration`, async () => {
    const rascalBroker = new RascalBroker(brokerConfig, logger);
    await rascalBroker.connect()

    rascalBrokerMock.publish.mockResolvedValue(subscriptionEvent)

    const messageMock: PublicationMessage = { "message": "test-payload", config: { queue: "test-queue-1", } }
    const publicationConfigMock = <IPublicationConfig>{
      queue: "test-queue",
      exchange: "test-exchange",
      routingKey: "routing-test",
      exchangeConfig: {
        type: "topic",
      },
    }

    const pub = rascalBroker.publish(publicationConfigMock, messageMock)

    process.nextTick(() => {
      subscriptionEvent.emit('success', "message-1")
    })

    expect(rascalBroker).toBeDefined()
    expect(brokerCreateMock).toHaveBeenCalled()
    expect(rascalBrokerMock.publish.mock.calls[0][2]).toEqual( { "exchange": "test-exchange", "options": {}, "exchangeConfig": { "type": "topic" }, "queue": "test-queue-1", "routingKey": "routing-test" } )
    expect(rascalBrokerMock.publish).toHaveBeenCalledWith('test-exchange', 'test-payload', rascalBrokerMock.publish.mock.calls[0][2])

    await expect(pub).resolves.toBe('message-1')
  })

  it(`should publish message failure`, async () => {

    const rascalBroker = new RascalBroker(brokerConfig, logger, traceUtil);
    await rascalBroker.connect()

    rascalBrokerMock.publish.mockResolvedValue(subscriptionEvent)

    const messageMock: PublicationMessage = { "msg": "hello" }
    const publicationConfigMock = <IPublicationConfig>{
      queue: "test-queue",
      exchange: "test-exchange",
      routingKey: "routing-test",
      exchangeConfig: {
        type: "topic",
      },
    }

    const pub = rascalBroker.publish(publicationConfigMock, messageMock)

    process.nextTick(() => {
      subscriptionEvent.emit('error', "error-message")
    })

    expect(rascalBroker).toBeDefined()
    expect(brokerCreateMock).toHaveBeenCalled()
    expect(rascalBrokerMock.publish).toHaveBeenCalledWith('test-exchange', { "msg": "hello" }, {
      queue: "test-queue", exchange: "test-exchange", routingKey: "routing-test", options: { headers: {} }, exchangeConfig: { type: "topic" },
    })

    process.nextTick(() => {
      expect(logger.error).toHaveBeenCalled()
      expect(logger.error.mock.calls[0][0]).toBe("error-message")
    })

    await expect(pub).rejects.toBe('error-message')
  })

  it(`should subscribe message acknowledge message successfully`, async (done) => {

    const messageCallbackMock = jest.fn()
    rascalBrokerMock.subscribe.mockResolvedValue(publicationEvent)
    traceUtil.startSpanFromHeader.mockReturnValue({} as any)


    const rascalBroker = new RascalBroker(brokerConfig, logger, traceUtil);
    await rascalBroker.connect()
    await rascalBroker.subscribe(subscriptionConfigMock, messageCallbackMock, {}, errorStrategyMock)

    expect(rascalBroker).toBeDefined()
    expect(brokerCreateMock).toHaveBeenCalled()
    expect(rascalBrokerMock.subscribe).toHaveBeenCalledWith('test-queue', { errorStrategy: { type: 'REJECT' } })

    publicationEvent.emit('message',
      messageSubscribeMock,
      contentMock,
      ackOrNackMock
    )

    process.nextTick(() => {
      expect(traceUtil.startSpanFromHeader).toHaveBeenCalledTimes(1)
      expect(messageCallbackMock.mock.calls[0][0]).toEqual(contentMock)
      expect(ackOrNackMock.mock.calls[0][0]).toBeUndefined()
      expect(traceUtil.finishSpan).toHaveBeenCalledTimes(1)
      done()
    });

  })

  it(`should subscribe message acknowledge message successfully when not set tracer`, async (done) => {

    const messageCallbackMock = jest.fn()
    rascalBrokerMock.subscribe.mockResolvedValue(publicationEvent)
    traceUtil.startSpanFromHeader.mockReturnValue({} as any)

    const rascalBroker = new RascalBroker(brokerConfig, logger);
    await rascalBroker.connect()
    await rascalBroker.subscribe(subscriptionConfigMock, messageCallbackMock, {}, errorStrategyMock)

    expect(rascalBroker).toBeDefined()
    expect(brokerCreateMock).toHaveBeenCalled()
    expect(rascalBrokerMock.subscribe).toHaveBeenCalledWith('test-queue', { errorStrategy: { type: 'REJECT' } })

    publicationEvent.emit('message',
      messageSubscribeMock,
      contentMock,
      ackOrNackMock
    )



    process.nextTick(() => {
      expect(traceUtil.startSpanFromHeader).not.toHaveBeenCalledTimes(1)
      expect(messageCallbackMock.mock.calls[0][0]).toEqual(contentMock)
      expect(ackOrNackMock.mock.calls[0][0]).toBeUndefined()
      expect(traceUtil.finishSpan).not.toHaveBeenCalledTimes(1)
      done()
    });

  })

  it(`should subscribe message but invokePromiseOrFunction throw exception`, async (done) => {

    const messageCallbackMock = jest.fn()
    rascalBrokerMock.subscribe.mockResolvedValue(publicationEvent)
    traceUtil.startSpanFromHeader.mockReturnValue({} as any)
    messageCallbackMock.mockRejectedValue({})


    const rascalBroker = new RascalBroker(brokerConfig, logger, traceUtil);
    await rascalBroker.connect()
    await rascalBroker.subscribe(subscriptionConfigMock, messageCallbackMock, {}, errorStrategyMock)

    expect(rascalBroker).toBeDefined()
    expect(brokerCreateMock).toHaveBeenCalled()
    expect(rascalBrokerMock.subscribe).toHaveBeenCalledWith('test-queue', { errorStrategy: { type: 'REJECT' } })

    publicationEvent.emit('message',
      messageSubscribeMock,
      contentMock,
      ackOrNackMock
    )

    process.nextTick(() => {
      expect(traceUtil.startSpanFromHeader).toHaveBeenCalledTimes(1)
      expect(messageCallbackMock).toHaveBeenCalled()
      expect(messageCallbackMock.mock.calls[0][0]).toEqual(contentMock)
      expect(handleOnMessageErrorMock).toHaveBeenCalled()
      expect(traceUtil.finishSpan).toHaveBeenCalledTimes(1)
      done()
    });

  })


  it(`should not consume an invalid massage when listener is bound to invalid_content`, async (done) => {

    const messageCallbackMock = jest.fn()
    rascalBrokerMock.subscribe.mockResolvedValue(publicationEvent)

    const rascalBroker = new RascalBroker(brokerConfig, logger);
    await rascalBroker.connect()
    await rascalBroker.subscribe(subscriptionConfigMock, messageCallbackMock, {}, errorStrategyMock)

    expect(rascalBroker).toBeDefined()
    expect(brokerCreateMock).toHaveBeenCalled()
    expect(rascalBrokerMock.subscribe).toHaveBeenCalledWith('test-queue', { errorStrategy: { type: 'REJECT' } })

    publicationEvent.emit('invalid_content',
      new SyntaxError("Unexpected token e in JSON at position 1"),
      "test-content",
      ackOrNackMock
    )

    process.nextTick(() => {
      expect(logger.error).toHaveBeenCalled()
      expect(handleOnMessageErrorMock).toHaveBeenCalled()
      done()
    });
  })

  it(`should not consume an invalid massage when listener is bound to error`, async (done) => {

    const messageCallbackMock = jest.fn()
    rascalBrokerMock.subscribe.mockResolvedValue(publicationEvent)
    const rascalBroker = new RascalBroker(brokerConfig, logger);
    await rascalBroker.connect()
    await rascalBroker.subscribe(subscriptionConfigMock, messageCallbackMock, {}, errorStrategyMock)

    expect(rascalBroker).toBeDefined()
    expect(brokerCreateMock).toHaveBeenCalled()
    expect(rascalBrokerMock.subscribe).toHaveBeenCalledWith('test-queue', { errorStrategy: { type: 'REJECT' } })

    publicationEvent.emit('error',
      new SyntaxError("Unexpected token e in JSON at position 1"),
      "test-content",
      ackOrNackMock
    )

    process.nextTick(() => {
      expect(logger.error).toHaveBeenCalled()
      done()
    });
  })

})




