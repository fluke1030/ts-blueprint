import { MetaDataRegistry } from '../metaDataRegistry';
import { IQueueConfig, ISubscriptionConfig } from '../rascalConfig';

export function amqpConsume(queueName: string, subConfig?: ISubscriptionConfig): Function;
export function amqpConsume(queueConfig: IQueueConfig, subConfig?: ISubscriptionConfig): Function;
export function amqpConsume(queueNameOrconfig: string | IQueueConfig, subConfig?: ISubscriptionConfig) {
  // tslint:disable-next-line: no-function-expression
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    if (typeof descriptor.value === 'function') {
      let config: IQueueConfig = {} as any;

      if (typeof queueNameOrconfig === 'string') {
        config.queueName = queueNameOrconfig;
      } else {
        config = queueNameOrconfig;
      }

      MetaDataRegistry.REGISTER_CONSUMER(config, target.constructor, propertyKey, subConfig);
    }
  }
}

