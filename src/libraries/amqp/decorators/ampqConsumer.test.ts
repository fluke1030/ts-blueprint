import { MetaDataRegistry } from '../metaDataRegistry';
import { SubscriptionMessage } from '../type';
import { amqpConsume } from './amqpConsumer';

jest.mock('../metaDataRegistry');

describe('Amqp consumer decorator', () => {
  // @ts-ignore
  class TestClass {
    @amqpConsume('testq')
    async testMethod(_message: SubscriptionMessage) { }

    @amqpConsume(
      { exchange: 'testexch', queueName: 'testq1' },
      { deferCloseChannel: 3000 })
    async testMethod1(_message: SubscriptionMessage) { }
  }

  it('should add new consumers', () => {
    const mockRegisterConsumer = MetaDataRegistry.REGISTER_CONSUMER as jest.Mock;

    expect(mockRegisterConsumer).toHaveBeenCalledTimes(2);
    assertCalledArgs(
      mockRegisterConsumer.mock.calls[0],
      [{ queueName: 'testq' }, TestClass, 'testMethod', undefined]);

    assertCalledArgs(
      mockRegisterConsumer.mock.calls[1],
      [{
        exchange: 'testexch',
        queueName: 'testq1'
      }, TestClass, 'testMethod1', {
        deferCloseChannel: 3000
      }]);
  })
})

function assertCalledArgs(actualArgs: any[], expectedArgs: any[]) {
  expect(actualArgs).toEqual(expectedArgs);
}