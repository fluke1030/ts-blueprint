import { AuthenticatedAuthorizationProvider } from './authenticatedProvider';
import { IAuthorizationRequest } from './type';

describe('Authenticated Authorization Provider', () => {

  it('should support', () => {
    const provider = new AuthenticatedAuthorizationProvider();
    const request: IAuthorizationRequest<any> = {
      authenticated: true
    };

    expect(provider.isSupported(request)).toBe(true);
  })

  it('should not support', () => {
    const provider = new AuthenticatedAuthorizationProvider();
    const request: IAuthorizationRequest<any> = {
      roles: []
    };

    expect(provider.isSupported(request)).toBe(false);
  })

  it('should authorized when user presences', () => {
    const provider = new AuthenticatedAuthorizationProvider();
    const request: IAuthorizationRequest<any> = {
      authenticated: true,
      user: {}
    };

    expect(provider.isAuthorized(request)).resolves.toBe(true);
  })

  it('should not authorized when user is not present', () => {
    const provider = new AuthenticatedAuthorizationProvider();
    const request: IAuthorizationRequest<any> = {
      authenticated: true,
    };

    expect(provider.isAuthorized(request)).resolves.toBe(false);
  })


})