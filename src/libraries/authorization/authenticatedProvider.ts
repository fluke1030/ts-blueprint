import { IAuthorizationProvider, IAuthorizationRequest } from './type';

export class AuthenticatedAuthorizationProvider implements IAuthorizationProvider<any> {

  isSupported(request: IAuthorizationRequest<any>): boolean {
    return request.authenticated === true;
  }

  async isAuthorized(request: IAuthorizationRequest<any>): Promise<boolean> {
    if (request.user) {
      return true;
    }

    return false;
  }
}