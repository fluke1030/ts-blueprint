import { mock, mockClear } from 'jest-mock-extended';
import { AggregateAuthorizationProvider } from './aggregateProvider';
import { IAuthorizationProvider } from './type';

describe('Aggregate Authorization Provider', () => {
  const mockProvider1 = mock<IAuthorizationProvider<any>>();
  const mockProvider2 = mock<IAuthorizationProvider<any>>();
  const provider = new AggregateAuthorizationProvider([mockProvider1, mockProvider2]);

  beforeEach(() => {
    mockClear(mockProvider1);
  })

  it('should support', () => {
    expect(provider.isSupported()).toBe(true);
  })

  it('should authorized when no supported provider', () => {
    mockProvider1.isSupported.mockReturnValue(false);

    expect(provider.isAuthorized({})).resolves.toBe(true);
  })

  it('should authorized when provider authorized', () => {
    mockProvider1.isSupported.mockReturnValue(true);
    mockProvider1.isAuthorized.mockResolvedValue(true);

    expect(provider.isAuthorized({})).resolves.toBe(true);
  })

  it('should authorized when one provider authorized', () => {
    mockProvider1.isSupported.mockReturnValue(true);
    mockProvider1.isAuthorized.mockResolvedValue(false);

    mockProvider2.isSupported.mockReturnValue(true);
    mockProvider2.isAuthorized.mockResolvedValue(true);

    expect(provider.isAuthorized({})).resolves.toBe(true);
  })

  it('should not authorized when all provider unauthorized', () => {
    mockProvider1.isSupported.mockReturnValue(true);
    mockProvider1.isAuthorized.mockResolvedValue(false);

    mockProvider2.isSupported.mockReturnValue(true);
    mockProvider2.isAuthorized.mockResolvedValue(false);

    expect(provider.isAuthorized({})).resolves.toBe(false);
  })


})