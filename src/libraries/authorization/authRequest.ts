import { IAuthorizationOption, IAuthorizationRequest } from './type';

export function buildAuthorizationRequest<U>(data: string[] | IAuthorizationOption[]): IAuthorizationRequest<U> {
  if (data.length === 0) {
    return { authenticated: true }
  }

  const firstElm = data[0];

  if (typeof firstElm === 'string') {
    return {
      roles: data
    } as IAuthorizationRequest<U>
  }


  const request: IAuthorizationRequest<U> = {};

  const { resource, scope } = firstElm;
  if (resource || scope) {
    request.permission = { resource: resource || '*', scope: scope || '*' };
  }

  return request
}