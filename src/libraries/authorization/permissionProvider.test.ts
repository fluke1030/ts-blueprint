import { mock, mockClear } from 'jest-mock-extended';
import { PermissionAuthorizationProvider } from './permissionProvider';
import { IAuthorizationRequest, IPermissionRepository } from './type';

interface IUser {
  roles: string[]
}

describe('Aggregate Authorization Provider', () => {
  const mockPermissionRepo = mock<IPermissionRepository<IUser>>();
  const provider = new PermissionAuthorizationProvider(mockPermissionRepo);

  beforeEach(() => {
    mockClear(mockPermissionRepo);
  })

  it('should support', () => {
    const request: IAuthorizationRequest<any> = {
      permission: { resource: '*', scope: '*' }
    };

    expect(provider.isSupported(request)).toBe(true);
  })

  it('should not support', () => {
    const request: IAuthorizationRequest<any> = {
    };

    expect(provider.isSupported(request)).toBe(false);
  })

  it('should unauthorized when no user in request', async () => {
    mockPermissionRepo.getPermissions.mockResolvedValue([]);

    const request: IAuthorizationRequest<any> = {
      permission: { scope: 'read', resource: 'test' }
    };

    const result = await provider.isAuthorized(request);

    expect(result).toBe(false);
  })

  it('should unauthorized when user has no permission', async () => {
    mockPermissionRepo.getPermissions.mockResolvedValue([]);

    const request: IAuthorizationRequest<any> = {
      user: {},
      permission: { scope: 'read', resource: 'test' }
    };

    const result = await provider.isAuthorized(request);

    expect(result).toBe(false);
  })

  it('should authorized when user has exact match permission', async () => {
    mockPermissionRepo.getPermissions.mockResolvedValue([
      { scope: 'read', resource: 'test' },
      { scope: 'write', resource: 'test2' },
    ]);

    const request: IAuthorizationRequest<any> = {
      user: {},
      permission: { scope: 'read', resource: 'test' }
    };

    const result = await provider.isAuthorized(request);

    expect(result).toBe(true);
  })

  it('should authorized when user has scope all permission', async () => {
    mockPermissionRepo.getPermissions.mockResolvedValue([
      { scope: '*', resource: 'test' },
    ]);

    const request: IAuthorizationRequest<any> = {
      user: {},
      permission: { scope: 'write', resource: 'test' }
    };

    const result = await provider.isAuthorized(request);

    expect(result).toBe(true);
  })

  it('should authorized when user has all resource permission', async () => {
    mockPermissionRepo.getPermissions.mockResolvedValue([
      { scope: 'read', resource: '*' },
    ]);

    const request: IAuthorizationRequest<any> = {
      user: {},
      permission: { scope: 'read', resource: 'test' }
    };

    const result = await provider.isAuthorized(request);

    expect(result).toBe(true);
  })

  it('should authorized when user has all resource permission (implicit)', async () => {
    mockPermissionRepo.getPermissions.mockResolvedValue([
      { scope: 'read', resource: '*' },
    ]);

    const request: IAuthorizationRequest<any> = {
      user: {},
      permission: { scope: 'read', resource: 'test' }
    };

    const result = await provider.isAuthorized(request);

    expect(result).toBe(true);
  })

  it('should authorized when user has any permission', async () => {
    mockPermissionRepo.getPermissions.mockResolvedValue([
      { scope: '*', resource: '*' },
    ]);

    const request: IAuthorizationRequest<any> = {
      user: {},
      permission: { scope: 'write', resource: 'test' }
    };

    const result = await provider.isAuthorized(request);

    expect(result).toBe(true);
  })


})