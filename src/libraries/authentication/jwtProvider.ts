import { AuthenticationError } from './errors';
import { verify } from './jwtUtil';
import { IAnonoymousableUser, IUserRepository } from './type';

export interface IJwtOptions {
  tokenHeaderKey?: string
  userIdKey?: string
  secret?: string
  publicKey?: string
}

export class JwtAuthenticationProvider<T extends IAnonoymousableUser> {
  private secretOrPublicKey = '';
  private tokenHeaderKey: string = '';
  private userIdKey: string = '';

  constructor(
    private userProvider: IUserRepository<T>,
    private options: IJwtOptions = {}) {

    const {
      tokenHeaderKey = 'authorization',
      userIdKey = 'sub',
      secret,
      publicKey } = options;

    this.tokenHeaderKey = tokenHeaderKey;
    this.userIdKey = userIdKey;
    this.secretOrPublicKey = secret || publicKey || '';

    if (!this.secretOrPublicKey) {
      throw new Error('Eeither secret or publicKey is required.');
    }
  }

  async getUser(headers: any): Promise<T | undefined> {
    const token: string = headers[this.tokenHeaderKey];

    if (!token) {
      return;
    }

    const decoded = this.verify(token.replace('Bearer ', ''));

    const userId = decoded[this.userIdKey];

    if (!userId) {
      throw new AuthenticationError(`no userId found for key '${this.options.userIdKey} in jwt token'`);
    }

    const user = await this.userProvider.getUserById(userId);

    return user;
  }

  verify(token: string): any {
    try {
      return verify(token, this.secretOrPublicKey);
    } catch (err) {
      throw new AuthenticationError(err);
    }
  }
}