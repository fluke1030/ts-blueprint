import { mock, mockClear } from 'jest-mock-extended';
import { AuthenticationProvider } from './authenticationProvider';
import { AuthenticationError } from './errors';
import { sign } from './jwtUtil';
import { IAnonoymousableUser, IUserRepository } from './type';

interface ITestUser extends IAnonoymousableUser { }

describe('Authentication Provider', () => {
  const mockUserRepo = mock<IUserRepository<ITestUser>>();

  let provider: AuthenticationProvider<ITestUser>;

  beforeEach(() => {
    mockClear(mockUserRepo);
  })

  describe('Jwt Authentication success', () => {
    it('should returns user when header contains authorization headers', async () => {
      mockUserRepo.getUserById.mockResolvedValue({ anonymous: false });

      const secret = 'testsecret';

      provider = new AuthenticationProvider(mockUserRepo, {
        useJwt: true, jwtOptions: {
          secret,
          userIdKey: 'userKey'
        }
      });

      const token = sign({ userKey: 'user1' }, secret);

      const result = await provider.getUserFromCtx({ headers: { 'authorization': `Bearer ${token}` } });

      expect(result).toBeTruthy();
      expect(result?.anonymous).toBe(false);
      expect(mockUserRepo.getUserById).toBeCalledWith('user1');
    })

    it('should returns no user when header contains no authorization headers', async () => {
      mockUserRepo.getUserById.mockResolvedValue({ anonymous: false });

      const secret = 'testsecret';

      provider = new AuthenticationProvider(mockUserRepo, {
        useJwt: true, jwtOptions: {
          secret,
          userIdKey: 'userKey'
        }
      });

      const result = await provider.getUserFromCtx({ headers: {} });

      expect(result).not.toBeDefined();
    })

  })


  describe('Jwt Authentication error', () => {
    it('should throw error when token is invalid', async () => {
      mockUserRepo.getUserById.mockResolvedValue({ anonymous: false });

      const secret = 'testsecret';

      provider = new AuthenticationProvider(mockUserRepo, {
        useJwt: true, jwtOptions: {
          secret,
        }
      });


      const check = () => {
        return provider.getUserFromCtx({ headers: { authorization: 'invalid token' } });
      }

      expect(check()).rejects.toThrowError(AuthenticationError);
    })

    it('should throw error when secret or public key not provided', async () => {
      const check = () => {
        provider = new AuthenticationProvider(mockUserRepo, { useJwt: true });
      }

      expect(check).toThrow();
    })

    it('should throw error when userKey not found', async () => {
      mockUserRepo.getUserById.mockResolvedValue({ anonymous: false });

      const secret = 'testsecret';

      provider = new AuthenticationProvider(mockUserRepo, {
        useJwt: true, jwtOptions: {
          secret,
          userIdKey: 'userKey'
        }
      });

      const token = sign({ key: 'user1' }, secret);

      const check = () => {
        return provider.getUserFromCtx({ headers: { 'authorization': `Bearer ${token}` } });
      }

      expect(check()).rejects.toThrowError(AuthenticationError);
    })

  })


})

