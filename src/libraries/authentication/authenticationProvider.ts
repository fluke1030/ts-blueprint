import { CustomHeaderAuthenticationProvider } from './customHeaderProvider';
import { AuthenticationError } from './errors';
import { JwtAuthenticationProvider } from './jwtProvider';
import { IAnonoymousableUser, IAuthenticationContext, IAuthenticationOptions, IUserRepository } from './type';

export class AuthenticationProvider<T extends IAnonoymousableUser> {
  private customHeaderProvider?: CustomHeaderAuthenticationProvider<T>;
  private jwtProvider?: JwtAuthenticationProvider<T>;

  constructor(
    userProvider: IUserRepository<T>,
    private options: IAuthenticationOptions = {
      allowAnonymous: false,
      useCustomHeader: false,
    }) {

    if (options.useCustomHeader) {
      this.customHeaderProvider = new CustomHeaderAuthenticationProvider<T>(userProvider, options.customHeaderOptions);
    }

    if (options.useJwt) {
      this.jwtProvider = new JwtAuthenticationProvider<T>(userProvider, options.jwtOptions);
    }
  }

  async getUserFromCtx(ctx: IAuthenticationContext) {
    let user: T | undefined;

    if (this.customHeaderProvider) {
      user = await this.customHeaderProvider.getUserFromCustomHeader(ctx.headers);
    }

    if (!user && this.jwtProvider) {
      user = await this.jwtProvider.getUser(ctx.headers);
    }

    if (!this.options.allowAnonymous && user && user.anonymous) {
      throw new AuthenticationError(`Anonymous user is not allowed`);
    }

    return user;
  }
}