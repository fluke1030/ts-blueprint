import * as jwt from 'jsonwebtoken';

export function sign(payload: object, secret: string) {
  return jwt.sign(payload, secret);
}

export function verify(token: string, secretOrPublicKey: string) {
  return jwt.verify(token, secretOrPublicKey)
}
