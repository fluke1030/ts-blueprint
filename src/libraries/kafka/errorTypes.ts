export class KafkaErrorRetry extends Error { }

export class KafkaErrorRetriesExhausted extends Error { }

export class KafkaInvalidTopic extends Error { }