import { Kafka, KafkaMessage, Message, Transaction } from 'kafkajs';
import { Service } from 'typedi';
import { IKafkaConnectionOptions } from '.';
import { handleOnMessageError } from '../kafka/errorHandler';
import { invokePromiseOrFunction, safeJSONParse } from '../kafka/util';
import { ILogger } from '../logger/logger.interface';
import { IKafkaConsumerConfig } from './decorators/kafkaConsumer';
import { IKafkaProducer } from './decorators/kafkaProducer';
import { KafkaInvalidTopic } from './errorTypes';
import { PayloadHandler } from './payloadHandler';


export interface IKafkaBroker {
  connect(): Promise<void>,
  toBufferJSON(value: any): Buffer,
  prepareProduceMessage(config: IKafkaProducer, messages: any): Message,
  serialize(config: IKafkaProducer, messages: ProduceMessage | ProduceMessage[]): Message[],
  deSerialize(message: KafkaMessage): IMessageConsumer,
  produceTransaction(producerConfig: IKafkaProducer, transactionalId: string, message: any | any[], callBackFunction: Function): Promise<void>
  produce(producerConfig: IKafkaProducer, message: any | any[]): Promise<void>,
  consume(consumerConfig: IKafkaConsumerConfig, originalFunction: Function, context: any): Promise<void>
}

export interface IMessageConsumer extends KafkaMessage {
  value: any,
  config?: IKafkaProducer
}

export type ProduceMessage = {
  message: any,
  config?: IKafkaProducer
}

export type ConsumeMessage = IMessageConsumer

@Service()
export class KafkaBroker implements IKafkaBroker {

  private broker!: Kafka

  constructor(
    private kafkaConfig: IKafkaConnectionOptions,
    private logger: ILogger,
  ) { }

  async connect() {
    const { topic_prefix, ...config } = this.kafkaConfig
    this.broker = new Kafka({
      ...config,
    })

    const producer = this.broker.producer()
    await producer.connect()
    this.logger.info({ event: 'kafka_broker_connected' }, `Successfully connect to kafka broker`);
  }

  toBufferJSON(value: any): Buffer {
    return Buffer.from(JSON.stringify(value));
  }

  prepareProduceMessage(config: IKafkaProducer, messages: any): Message {
    let value = messages

    if (typeof messages == "object" && 'message' in messages) {
      config.options = messages.config
      value = messages.message
    }

    let payload = { value: this.toBufferJSON(value) }

    payload = Object.assign({}, payload, { ...config.options })

    return payload
  }


  serialize(config: IKafkaProducer, messages: ProduceMessage | ProduceMessage[]): Message[] {

    let messageSerialize: Message[] = []

    if (Array.isArray(messages)) {
      for (const message of messages) {
        const payload = this.prepareProduceMessage(config, message)
        messageSerialize.push(payload)
      }

    } else {
      const payload = this.prepareProduceMessage(config, messages)
      messageSerialize = [payload]
    }
    return messageSerialize
  }

  async produceTransaction(
    producerConfig: IKafkaProducer,
    transactionalId: string,
    message: any | any[],
    callBackFunction: Function
  ) {

    const producer = this.broker.producer({
      maxInFlightRequests: 1,
      idempotent: true,
      transactionalId
    })


    await producer.connect()
    const transaction: Transaction = await producer.transaction()
    const messages = this.serialize(producerConfig, message)

    if (!!!producerConfig.topic) {
      throw new KafkaInvalidTopic("Invalid topic for produce")
    }

    try {
      await transaction.send({
        topic: producerConfig.topic,
        messages
      })

      await callBackFunction()

      transaction.commit()
      this.logger.info({ event: "kafka_transaction_commit", topic: producerConfig.topic })

    } catch (error) {
      await transaction.abort()
      this.logger.error(error, { event: "kafka_transaction_rollback", })
      throw error

    }
  }


  async produce(producerConfig: IKafkaProducer, message: any | any[]) {
    const { config, options } = producerConfig
    const topic: string = `${this.kafkaConfig.topic_prefix}${producerConfig.topic}`
    const messages: Message[] = this.serialize(producerConfig, message);

    await this.broker.producer(config).send({
      topic,
      messages,
      ...options
    })

  }

  deSerialize(message: KafkaMessage): IMessageConsumer {

    let messageDeSerialize: IMessageConsumer = message

    if (message.value instanceof Buffer) {
      const plainMessage = message.value.toString()
      const { value } = safeJSONParse(plainMessage)
      messageDeSerialize = Object.assign({}, messageDeSerialize, { value: value })
    }

    if (message.key instanceof Buffer) {
      const plainMessage = message.key.toString()
      const { value } = safeJSONParse(plainMessage)
      messageDeSerialize = Object.assign({}, messageDeSerialize, { key: value })
    }

    return messageDeSerialize
  }

  async consume(consumerConfig: IKafkaConsumerConfig, originalFunction: Function, context: any) {
    const consumer = this.broker.consumer(consumerConfig)
    await consumer.connect()
    await consumer.subscribe(
      consumerConfig.subscribe
    )

    await consumer.run({
      autoCommit: false,
      eachMessage: async (payload) => {

        // TODO: Payload handler Layer
        const payloadHandler = new PayloadHandler(
          consumerConfig,
          consumer,
          payload
        )

        const { topic, partition, message } = payload
        const messageDeSerialize = this.deSerialize(message)

        try {

          await invokePromiseOrFunction(originalFunction, context, [messageDeSerialize])
          await payloadHandler.commit()

          this.logger.info({
            event: "kafka_commit",
            topic,
            partition,
            message
          })


        } catch (error) {

          await handleOnMessageError(
            error,
            payloadHandler,
            this.logger,
            this,
            consumerConfig.errorStrategy,
          )
        }
      }
    })

  }
}

