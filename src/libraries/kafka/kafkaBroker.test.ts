import { mock, mockClear } from 'jest-mock-extended';
import { Consumer, EachMessagePayload, Kafka, Transaction } from 'kafkajs';
import { Producer } from 'kafkajs/types';
import { IKafkaConnectionOptions } from '.';
import { ILogger } from '../logger/logger.interface';
import * as errorHandler from "./errorHandler";
import { KafkaInvalidTopic } from './errorTypes';
import { KafkaBroker } from "./kafkaBroker";

jest.mock('./errorHandler.ts')

const kafkaMock = mock<Kafka>()

jest.mock('kafkajs', () => ({ Kafka: jest.fn().mockImplementation(() => kafkaMock) }))

describe("Kafka Broker", () => {

  const callbackFunctionMock = jest.fn()

  beforeEach(() => {
    mockClear(kafkaMock)
    callbackFunctionMock.mockClear()
  })

  it('should create kafka broker successfully', async () => {

    const kafkaConfig = mock<IKafkaConnectionOptions>();
    const logger = mock<ILogger>();

    const kafkaBroker = new KafkaBroker(
      kafkaConfig,
      logger
    )

    expect(kafkaBroker).toBeDefined();
    expect(kafkaBroker instanceof KafkaBroker).toBeTruthy()
  });

  it("should broker connect successfully", async () => {
    const kafkaConfig = mock<IKafkaConnectionOptions>();
    const logger = mock<ILogger>();

    const producerMock = mock<Producer>()
    kafkaMock.producer.mockReturnValue(producerMock)

    const kafkaBroker = new KafkaBroker(
      kafkaConfig,
      logger
    )

    await kafkaBroker.connect()
    expect(producerMock.connect).toHaveBeenCalled()

  })

  it("should produce message successfully", async () => {

    const kafkaConfig = mock<IKafkaConnectionOptions>();
    const logger = mock<ILogger>();

    const producerMock = mock<Producer>()
    kafkaMock.producer.mockReturnValue(producerMock)

    const kafkaBroker = new KafkaBroker(
      kafkaConfig,
      logger
    )

    await kafkaBroker.connect()
    await kafkaBroker.produce(
      {
        topic: "test-topic",
      },
      "test",
    )

    expect(producerMock.send).toHaveBeenCalled()

  })

  it("should produce list message and override config successfully", async () => {

    const kafkaConfig = mock<IKafkaConnectionOptions>();
    const logger = mock<ILogger>();

    const producerMock = mock<Producer>()
    kafkaMock.producer.mockReturnValue(producerMock)

    const kafkaBroker = new KafkaBroker(
      kafkaConfig,
      logger
    )

    await kafkaBroker.connect()
    await kafkaBroker.produce(
      { topic: "test-topic" },
      [
        {
          message: "test",
          config: {
            headers: { "test-id": 1 },
            acks: 1,
            timeout: 1000
          }
        },
        {
          message: "test2",
          config: {
            headers: { "test-id": 2 },
            acks: -1,
            timeout: 1000
          }
        }
      ]
    )

    expect(producerMock.send).toHaveBeenCalled()
    expect(producerMock.send.mock.calls[0][0].messages).toHaveLength(2)

  })

  it("should produce message transaction successfully ", async () => {

    const kafkaConfig = mock<IKafkaConnectionOptions>();
    const logger = mock<ILogger>();
    const producerMock = mock<Producer>()
    const transactionMock = mock<Transaction>()

    kafkaMock.producer.mockReturnValue(producerMock)
    producerMock.transaction.mockResolvedValue(transactionMock)



    callbackFunctionMock.mockResolvedValue({})

    const kafkaBroker = new KafkaBroker(kafkaConfig, logger)
    await kafkaBroker.connect()

    await kafkaBroker.produceTransaction(
      { topic: "test-topic" },
      "test-transaction-id",
      {
        message: "test",
      },
      callbackFunctionMock
    )
    expect(transactionMock.send.mock.calls[0][0].topic).toBe("test-topic")
    expect(transactionMock.commit).toHaveBeenCalled()
  })

  it("should rollback when produce transaction failed", async () => {

    const kafkaConfig = mock<IKafkaConnectionOptions>();
    const logger = mock<ILogger>();
    const producerMock = mock<Producer>()
    const transactionMock = mock<Transaction>()

    kafkaMock.producer.mockReturnValue(producerMock)
    producerMock.transaction.mockResolvedValue(transactionMock)


    callbackFunctionMock.mockRejectedValue(new Error("Test error"))

    const kafkaBroker = new KafkaBroker(kafkaConfig, logger)
    await kafkaBroker.connect()

    expect.assertions(1)

    try {
      await kafkaBroker.produceTransaction(
        { topic: "test-topic" },
        "test-transaction-id",
        { message: "test", },
        callbackFunctionMock
      )

    } catch (error) {

      expect(transactionMock.abort).toHaveBeenCalled()

    }
  })

  it("should throw KafkaInvalidTopic when invalid topic", async () => {

    const kafkaConfig = mock<IKafkaConnectionOptions>();
    const logger = mock<ILogger>();
    const producerMock = mock<Producer>()
    const transactionMock = mock<Transaction>()

    kafkaMock.producer.mockReturnValue(producerMock)
    producerMock.transaction.mockResolvedValue(transactionMock)


    const kafkaBroker = new KafkaBroker(kafkaConfig, logger)
    await kafkaBroker.connect()

    expect(kafkaBroker.produceTransaction(
      { topic: "" },
      "test-transaction-id",
      { message: "test", },
      callbackFunctionMock
    )).rejects.toThrowError(KafkaInvalidTopic)

  })

  it("should consume message successfully", async () => {

    const kafkaConfig = mock<IKafkaConnectionOptions>();
    const logger = mock<ILogger>();
    const producerMock = mock<Producer>()
    const consumerMock = mock<Consumer>()

    kafkaMock.producer.mockReturnValue(producerMock)
    kafkaMock.consumer.mockReturnValue(consumerMock)

    const kafkaBroker = new KafkaBroker(kafkaConfig, logger)
    await kafkaBroker.connect()

    let eachMessageMock: ((payload: EachMessagePayload) => Promise<void>) | undefined

    consumerMock.run.mockImplementation(async (consumerConfig) => {
      eachMessageMock = consumerConfig?.eachMessage
    })


    await kafkaBroker.consume({
      groupId: "test-group-id",
      subscribe: {
        topic: "test-consume"
      }
    },
      callbackFunctionMock,
      'consumer'
    )

    const mockPayload = {
      topic: "test-topic",
      partition: 0,
      message: {
        key: Buffer.from("test-key"),
        headers: { "test-id": "1" },
        value: Buffer.from(JSON.stringify({ test: "test" })),
        timestamp: "1",
        size: 10,
        attributes: 10,
        offset: "0",
      }
    }

    await eachMessageMock!(mockPayload)
    expect(consumerMock.connect).toHaveBeenCalled()
    expect(consumerMock.subscribe.mock.calls[0][0].topic).toBe("test-consume")

  })

  it("should invoke error handler when error while consuming a message", async () => {

    const kafkaConfig = mock<IKafkaConnectionOptions>();
    const logger = mock<ILogger>();
    const producerMock = mock<Producer>()
    const consumerMock = mock<Consumer>()

    kafkaMock.producer.mockReturnValue(producerMock)
    kafkaMock.consumer.mockReturnValue(consumerMock)

    const kafkaBroker = new KafkaBroker(kafkaConfig, logger)
    await kafkaBroker.connect()

    let eachMessageMock: ((payload: EachMessagePayload) => Promise<void>) | undefined

    consumerMock.run.mockImplementation(async (consumerConfig) => {
      eachMessageMock = consumerConfig?.eachMessage
    })


    callbackFunctionMock.mockRejectedValue({})

    await kafkaBroker.consume({
      groupId: "test-group-id",
      subscribe: {
        topic: "test-consume"
      }
    },
      callbackFunctionMock,
      'consumer'
    )

    const mockPayload = {
      topic: "test-topic",
      partition: 0,
      message: {
        key: Buffer.from("test-key"),
        headers: { "test-id": "1" },
        value: Buffer.from(JSON.stringify({ test: "test" })),
        timestamp: "1",
        size: 10,
        attributes: 10,
        offset: "0",
      }
    }

    await eachMessageMock!(mockPayload)

    const handleOnMessageErrorMock = errorHandler.handleOnMessageError as jest.Mock
    expect(consumerMock.connect).toHaveBeenCalled()
    expect(consumerMock.subscribe.mock.calls[0][0].topic).toBe("test-consume")
    expect(callbackFunctionMock).toHaveBeenCalled()
    expect(handleOnMessageErrorMock).toHaveBeenCalled()
  })


})