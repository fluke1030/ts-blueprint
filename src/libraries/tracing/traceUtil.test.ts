import { ILocalTracer } from 'hpropagate/lib/tracer';
import { mock } from 'jest-mock-extended';
import { IRouterContext } from 'koa-router';
import { Span, SpanContext, Tags, Tracer } from 'opentracing';
import { ITracingInfo } from './interface';
import { TraceUtil } from "./traceUtil";

class MockSpan extends Span {
  tags: { [key: string]: any; } = {};
  logs: { [key: string]: any; } = {};
  isFinish: boolean = false;

  constructor(private ctx: SpanContext) {
    super();
  }

  setTag(key: string, value: any) {
    this.tags[key] = value;
    return this;
  }

  log(keyValues: { [key: string]: any }) {
    this.logs = { ...this.logs, ...keyValues };
    return this;
  }

  context() {
    return this.ctx;
  }

  finish() {
    this.isFinish = true;
  }

}

class MockContext extends SpanContext {
  constructor(private traceId: string, private spanId: string) {
    super();
  }

  toSpanId() {
    return this.spanId;
  }

  toTraceId() {
    return this.traceId;
  }

  toString() {
    return `${this.traceId}:${this.spanId}`
  }
}

function mockTraceInfo(
  span: Span,
  parentCtx: SpanContext | null,
  currentLocalTraceCtx?: Map<any, any>
) {
  const globalTracer = mock<Tracer>();
  const localTracer: ILocalTracer = {
    currentTrace: {
      context: currentLocalTraceCtx
    },
    newTrace: () => {
      localTracer.currentTrace = { context: new Map() };
      return localTracer.currentTrace;
    }
  } as any;

  const traceInfo: ITracingInfo = {
    globalTracer,
    localTracer,
    headersToPropagate: ['localTraceKey1', 'globalTraceKey1'],
    localTracingKey: 'localTraceKey1',
    globalTracingKey: 'globalTraceKey1'
  }

  globalTracer.extract.mockReturnValue(parentCtx);
  globalTracer.startSpan.mockReturnValue(span);

  return traceInfo;
}

describe('traceUtil.test', () => {

  describe('Tracing from http ctx', () => {

    it('should start and finish span from ctx', () => {
      const mockSpan = new MockSpan(new MockContext('trace1', 'span1'));
      const localCtx = new Map([
        ['localTraceKey1', 'localtrace1'],
        ['globalTraceKey1', 'globaltrace1'],
      ])

      const mockInfo = mockTraceInfo(mockSpan, null, localCtx);

      const trace = new TraceUtil(mockInfo);

      const ctx = {
        url: '/testurl',
        method: 'get'
      } as IRouterContext;

      // start span
      const span = trace.startSpanFromCtx(ctx);

      expect(span).toBeDefined();
      expect(mockSpan.tags).toEqual({
        'http.method': 'get',
        'http.url': '/testurl',
        'localTraceKey1': 'localtrace1'
      });
      expect(trace.getCurrentSpan()).toBe(mockSpan);
      expect(trace.getGlobalTracingId()).toBe(`trace1:span1`);
      expect(trace.getLocalTracingId()).toBe('localtrace1');

      // finish span
      ctx.status = 200;
      trace.finishSpanFromCtx(span, ctx);
      expect(mockSpan.tags[Tags.HTTP_STATUS_CODE]).toBe('200');
      expect(mockSpan.isFinish).toBe(true);
    })

    it('should start and finish span from ctx with error', () => {
      const mockSpan = new MockSpan(new MockContext('trace1', 'span1'));
      const localCtx = new Map([
        ['localTraceKey1', 'localtrace1'],
        ['globalTraceKey1', 'globaltrace1'],
      ])

      const mockInfo = mockTraceInfo(mockSpan, null, localCtx);

      const trace = new TraceUtil(mockInfo);

      const ctx = {
        url: '/testurl',
        method: 'get'
      } as IRouterContext;

      // start span
      const span = trace.startSpanFromCtx(ctx);

      expect(span).toBeDefined();

      // finish span
      trace.finishSpanFromCtx(span, ctx, new Error('trace error'));
      expect(mockSpan.isFinish).toBe(true);
      expect(mockSpan.tags['error']).toBe(true);
      expect(mockSpan.logs).toMatchObject({ event: "error", message: "trace error" });
    })

  })

  describe('Tracing from headers', () => {
    it('should start and finish span from headers with propagated context', () => {
      // prepare
      const parentContext = new MockContext('trace1', 'span0');
      const mockSpan = new MockSpan(new MockContext('trace1', 'span1'));

      const mockInfo = mockTraceInfo(mockSpan, parentContext, undefined);

      // execute test
      const trace = new TraceUtil(mockInfo);

      const name = 'test_trace';
      const headers = {
        key1: 'value1',
        key2: 'value2',
        localTraceKey1: 'localtrace1'
      };

      // start span
      const span = trace.startSpanFromHeader(name, headers, {
        tags: { tag1: 'tagvalue1' },
        logs: [{ log1: 'logvalue1' }]
      });

      // assert test
      expect(span).toBeDefined();
      expect(trace.getCurrentSpan()).toBe(mockSpan);
      expect(trace.getGlobalTracingId()).toBe(`trace1:span1`);
      expect(trace.getLocalTracingId()).toBe('localtrace1');
      expect(mockSpan.tags).toEqual({
        localTraceKey1: 'localtrace1',
        tag1: 'tagvalue1'
      })
      expect(mockSpan.logs).toEqual({
        log1: 'logvalue1'
      })
      expect(mockSpan.isFinish).toBe(false);

      // finish span
      trace.finishSpan(span!);
      expect(mockSpan.isFinish).toBe(true);
    })
  })

  it('should start and finish span from headers without propagated context', () => {
    // prepare
    const parentContext = new MockContext('trace1', 'span0');
    const mockSpan = new MockSpan(new MockContext('trace1', 'span1'));

    const mockInfo = mockTraceInfo(mockSpan, parentContext, undefined);

    // execute test
    const trace = new TraceUtil(mockInfo);

    const name = 'test_trace';
    const headers = {
      key1: 'value1',
      key2: 'value2',
    };

    // start span
    const span = trace.startSpanFromHeader(name, headers);

    // assert test
    expect(span).toBeDefined();
    expect(trace.getCurrentSpan()).toBe(mockSpan);
    expect(trace.getGlobalTracingId()).toBe(`trace1:span1`);
    expect(trace.getLocalTracingId()).toBeDefined();
    expect(mockSpan.tags['localTraceKey1']).toBeDefined();
  })

  it('should start and finish with error', () => {
    // prepare
    const parentContext = new MockContext('trace1', 'span0');
    const mockSpan = new MockSpan(new MockContext('trace1', 'span1'));

    const mockInfo = mockTraceInfo(mockSpan, parentContext, undefined);

    // execute test
    const trace = new TraceUtil(mockInfo);

    const name = 'test_trace';
    const headers = {
      key1: 'value1',
      key2: 'value2',
    };

    // start span
    const span = trace.startSpanFromHeader(name, headers);

    // assert test
    expect(span).toBeDefined();
    expect(trace.getCurrentSpan()).toBe(mockSpan);
    expect(trace.getLocalTracingId()).toBeDefined();
    expect(mockSpan.tags['localTraceKey1']).toBeDefined();
    expect(mockSpan.isFinish).toBe(false);

    // finish span
    trace.finishSpan(span!, new Error('trace error'));
    expect(mockSpan.isFinish).toBe(true);
    expect(mockSpan.tags['error']).toBe(true);
    expect(mockSpan.logs).toMatchObject({ event: "error", message: "trace error" });
  })

  it('should not start without current span', () => {
    // prepare
    const mockSpan = new MockSpan(new MockContext('trace1', 'span1'));

    const mockInfo = mockTraceInfo(mockSpan, null, undefined);

    // execute test
    const trace = new TraceUtil(mockInfo);

    const name = 'test_trace';
    const headers = {
      key1: 'value1',
      key2: 'value2',
      localTraceKey1: 'localtrace1'
    };

    // start span
    const span = trace.startSpanFromHeader(name, headers);

    // assert test
    expect(span).toBeUndefined();
  })

  describe('Inject context to headers', () => {
    // prepare
    const localCtx = new Map([
      ['localTraceKey1', 'localtrace1'],
      ['globalTraceKey1', 'globaltrace1'],
    ])

    const mockSpan = new MockSpan(new MockContext('trace1', 'span1'));
    const mockInfo = mockTraceInfo(mockSpan, null, localCtx);

    // execute test
    const trace = new TraceUtil(mockInfo);

    const headers = {
      key1: 'value1',
      key2: 'value2',
    };

    trace.setCurrentSpan(mockSpan);

    // start span
    trace.injectTracingContext(headers);

    // assert test
    expect(headers).toEqual({
      localTraceKey1: 'localtrace1',
      globalTraceKey1: 'trace1:span1',
      key1: 'value1',
      key2: 'value2'
    });

    expect(mockInfo.globalTracer.inject).toBeCalled();
  })

})